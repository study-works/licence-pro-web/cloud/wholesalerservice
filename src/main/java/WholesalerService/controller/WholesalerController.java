package WholesalerService.controller;

import WholesalerService.exception.BadCorrelationException;
import WholesalerService.exception.BookNotFoundException;
import WholesalerService.exception.UnauthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;

import java.util.Map;

@RestController
public class WholesalerController {
    private static final String increaseStockUrl = "https://lpro-cloud-stock-service.oa.r.appspot.com/increaseStock?isbn={isbn}&amount={amount}&correlation={correlation}";
    private static final  int authorizationKey = 4589;

    @GetMapping(value = "/wholesaleBook", produces = "application/json")
    public @ResponseBody ResponseEntity<Map> wholesaleBook(@RequestParam(value="key") int key, @RequestParam(value="isbn") Long isbn, @RequestParam(value="amount") int amount, @RequestParam(value="correlation") Long correlation) throws Exception {
        try {
            RestTemplate restTemplate = new RestTemplate();

            if (key != authorizationKey) {
                throw new UnauthorizedException("The key is not valid!");
            }

            int quantity = amount / 5;
            if (amount % 5 != 0) quantity++;
            quantity = quantity * 5;

            ResponseEntity<Map> increaseStockResponse = restTemplate.getForEntity(increaseStockUrl, Map.class, isbn, quantity, correlation);

            Long increaseStockCorrelation = Long.valueOf((Integer) increaseStockResponse.getBody().get("correlation"));

            if (!increaseStockCorrelation.equals(correlation)) {
                throw new BadCorrelationException("The correlation is not the same");
            }

            return increaseStockResponse;
        } catch (HttpClientErrorException hcee) {
            switch (hcee.getStatusCode()) {
                case NOT_FOUND:
                    throw new BookNotFoundException("ISBN does not exist!");
                case BAD_REQUEST:
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                case CONFLICT:
                    throw hcee;
                default:
                    throw new Exception();
            }
        }
    }

    @GetMapping(value = "/helloworld", produces = "text/html")
    public @ResponseBody String displayHelloWorld(){
        return "Hello world";
    }
}
