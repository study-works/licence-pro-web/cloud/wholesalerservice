package WholesalerService.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "The correlation is not the same")
public class BadCorrelationException extends RuntimeException {
    public BadCorrelationException(String msg) {
        super(msg);
    }
}
